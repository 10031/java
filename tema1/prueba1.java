private class CalcularTiempo(Int32 tsegundos)
{
    Int32 horas = (tsegundos / 3600);
    Int32 minutos = ((tsegundos-horas*3600)/60);
    Int32 segundos = tsegundos-(horas*3600+minutos*60);
    return horas.ToString() + ":" + minutos.ToString() + ":" + segundos.ToString();
}