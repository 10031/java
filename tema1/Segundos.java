import java.io.*;
public class Segundos{
     public static void main(String[] args)throws IOException{
          BufferedReader lectura= new BufferedReader (new InputStreamReader(System.in));
          int t, seg=0, min=0, hora=0;

          System.out.print("Introduce segundos: ");
          t=Integer.parseInt(lectura.readLine());
          seg=t;
          min=t/60;
          hora=min/60;
          System.out.println("Los segundos en horas son: "+hora);
          System.out.println("Los segundos en minutos son: "+min);
          System.out.println("Los segundos en segundos son: "+seg);
        }
}
