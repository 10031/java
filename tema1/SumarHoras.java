public class SumarHoras {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String horaIni = "3600";
        String minutoIni = "60";
        String horaFin = "08";
        String minutoFin = "60";

        /* HORARIO INICIO*/
        // Convertimos hora en double
        double horaIniDouble = Integer.parseInt(horaIni);
        // Pasamos la hora a segundos
        double segundoHoraIni = horaIniDouble * 3600;
        // Convertimos minuto a segundos
        double minutoIniDouble = Integer.parseInt(minutoIni);
        double segundoMinutoIni = minutoIniDouble * 60;
        // Sumamos los segundos
        double totalSegundoIni = segundoHoraIni + segundoMinutoIni;

        /* HORARIO FIN*/
        // Convertimos hora en double
        double horaFinDouble = Integer.parseInt(horaFin);
        // Pasamos la hora a segundos
        double segundoHoraFin = horaFinDouble * 3600;
        // Convertimos minuto a segundos
        double minutoFinDouble = Integer.parseInt(minutoFin);
        double segundoMinutoFin = minutoFinDouble * 60;
        // Sumamos los segundos
        double totalSegundoFin = segundoHoraFin + segundoMinutoFin;

        
        double totalHora = totalSegundoFin - totalSegundoIni;
        
        // Imprime los segundos totales
        System.out.println("Tiempo trabajado total en SEGUNDOS: " + totalHora);

        /**
         * *** Por si hay cambio de día ****
         */
     
        double horaEnDosDias = 0;
        double minutosEnDosDias = 0;
        double segundosEnDosDias = 0;
        double segundosEnDosDias2 = 0;
        if (totalSegundoIni > totalSegundoFin) {  
            segundosEnDosDias = ((86400 - totalSegundoIni) + totalSegundoFin);
            System.out.println("Segundos en dos días: " + segundosEnDosDias);

            horaEnDosDias = Math.floor(segundosEnDosDias / 3600);
            minutosEnDosDias = Math.floor((segundosEnDosDias % 3600) / 60);
            segundosEnDosDias2 = Math.floor((segundosEnDosDias % 3600) % 60);

/**** También se podía haber calcualado así, pero es más lento
        while (segundos >= 3600) {
            contadorh++;
            segundos = segundos - 3600;
        }
       
        System.out.println(contadorh);
        
        while (segundos >= 60) {
            contadorm++;
            segundos = segundos - 60;
        }
        System.out.println(contadorm);
        System.out.println(segundos);
        ****/


            System.out.println("Horas trabajadas en dos días: " + horaEnDosDias);
            System.out.println("Minutos trabajados en dos días: " + minutosEnDosDias);
            System.out.println("Segundos trabajados en dos días: " + segundosEnDosDias2);

        }
        // Averiguar cuantos minutos son
        double minutos = 0;
        double hora = 0;
        while (totalHora >= 60) {
            minutos = minutos + 1;
            totalHora = totalHora - 60;
            if (minutos == 60) {
                hora = hora + 1;
                minutos = 0;
            }
        }
        System.out.println("Horas en un día: " + hora);
        System.out.println("Minutos en un día: " + minutos);
    }

}