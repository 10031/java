import java.util.Scanner;
public class Billetes {
	public static void main(String []args) {
		int cantidad;
		int b500, b200, b100, b50, b20, b10, b5, resto;
		Scanner s;

		if (args.length==0) {
			System.out.println("Introduce la cantidad: ");
			s=new Scanner(System.in);
			cantidad=s.nextInt();
		} else {
			cantidad=Integer.parseInt(args[0]);
		}



		b500=cantidad/500;
		resto=cantidad%500;
		b200=resto/200;
		resto=resto%200;
		b100=resto/100;
		resto=resto%100;
		b50=resto/50;
		resto=resto%50;
		b20=resto/20;
		resto=resto%20;
		b10=resto/10;
		resto=resto%10;
		b5=resto/5;
		resto=resto%5;
		if (b500>0) {
			System.out.println("Billetes de 500: "+b500);
		}
		if (b200>0) {
			System.out.println("Billetes de 200: "+b200);
		}
		if (b100>0) {
			System.out.println("Billetes de 100: "+b100);
		}
		if (b50>0) {
			System.out.println("Billetes de 50: "+b50);
		}
		if (b20>0) {
			System.out.println("Billetes de 20: "+b20);
		}
		if (b10>0) {
			System.out.println("Billetes de 10: "+b10);
		}
		if (b5>0) {
			System.out.println("Billetes de 5: "+b5);
		}
		if (resto>0) {
			System.out.println("Sobran "+resto);
		}
	}
}