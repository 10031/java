public class Dias{
	public static void main(String args[]){
		int day=Integer.parseInt(args[0]);
		String dayString;
		if (day<1 || day>7) {
			System.out.println("Necesito un parámetro en el rango [1,7]");
			System.exit(1);
		}

		switch (day)
		{
			case 1: dayString ="Lunes";
			  		break;
			case 2: dayString ="Martes";
			  		break;
			case 3: dayString ="Miércoles";
			  		break;
			case 4: dayString ="Jueves";
			  		break;
			case 5: dayString = "Viernes";
			  		break;
			case 6: dayString ="Sábado";
			  		break;  	
			default: dayString ="Domingo"; break;
		}
		System.out.println(dayString);
	}
		
}
