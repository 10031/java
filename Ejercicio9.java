import java.util.Scanner;
public class Ejercicio9{
    public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		int n, cifras=0;
        System.out.print("Introduce un número entero: ");
        n = sc.nextInt();
        if(n==0){
            cifras=1;
        }

        for(;n!=0;cifras++){
            n=n/10;
        }
        System.out.println("El número tiene " + cifras+ " cifras"); 
	}
}