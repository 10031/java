public class Ejercicio6{
	public static void main(String args[]){
		int resultado;
		int n=Integer.parseInt(args[0]);
		long inicio=System.currentTimeMillis();
		for(int i=2; i<=n; i++){
			for(int j=1; j<=10; j++){
				resultado= i*j;
				System.out.println(i+ "*"+j+ "="+ resultado);
			}
		}
		long tiempo=System.currentTimeMillis()-inicio;
		System.out.println("He tardado en ejecutarme: " +tiempo);
	}
}
	
