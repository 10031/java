import java.util.Scanner;
public class Ejercicio91{
	public static void main(String args[]){
	Scanner sc=new Scanner(System.in);
	System.out.print("Introduce un número entero: ");
    int numero = sc.nextInt();
	int digitos = (int)(Math.log10(numero)+1);
	System.out.println("El número tiene " +digitos+ " digitos");
	}
}