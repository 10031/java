public class Ejercicio12{
	public static void main(String args[]){
		int n=Integer.parseInt(args[0]);
		int temp, resto, sum=0, digitos=0;

        for(temp=n;temp!=0;digitos++){
            temp=temp/10;
        }
        
        for(temp=n;temp!=0;){
            resto=temp%10;
            sum = sum + (int)Math.pow(resto, digitos);
            temp = temp/10;
        }
		if(n == sum){
			System.out.println(n + " es un número de Armstrong.");
		}else{
			System.out.println(n + " no es un número de Armstrong.");
		}
	}
}