public class Ejercicio15 {
	public static void main (String args[]) {
			java.util.Scanner Leer = new java.util.Scanner(System.in);

            int dia = -1;

            int mes = -1;

            int anio = -1;

            int resultado = 0;

            String fecha;

 
            System.out.println("Introduce fecha de tu nacimiento: ");

            boolean fechaIncorrecta = false;

            do{

                System.out.print("Dia: ");

                dia = Leer.nextInt();

              System.out.print("Mes: ");

                mes = Leer.nextInt();

               System.out.print("Anio: ");

                anio = Leer.nextInt();

                fechaIncorrecta = (anio > 9999) || (dia < 1 || dia > 31) || (mes < 1 || mes > 12);

                if (fechaIncorrecta)

                    System.out.println("La fecha introducida no es correcta");

            }while (fechaIncorrecta);

            resultado = dia + mes + anio;

            do {

                resultado = resultado / 10 + resultado % 10;

            }while(resultado > 10);

            System.out.println(+resultado);

	}

}